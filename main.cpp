#include <iostream>
#include <boost/thread.hpp>

// This is an example of how to make threads wait on the previous thread once they reach a certain point
int main()
{
	std::map<boost::thread::id, boost::shared_ptr<boost::thread>> threads;
	boost::shared_ptr<boost::thread> prevThread(new boost::thread());

	int threadCount = 10;
	for(int i = 0; i < threadCount; i++)
	{
		uint sleep = ((i % 2) + 1) * 60;

		boost::shared_ptr<boost::thread> thread( new boost::thread([=, &prevThread, &threads] ()
		{
			std::cout << "Sleep(" << i << "): " << sleep << std::endl;

			// Do work
			boost::this_thread::sleep_for(boost::chrono::milliseconds(sleep));

			// This thread waits for the previous thread to finish before doing more work
			prevThread->join();

			// Work block 2
			std::cout << "Thread ID: " << i << std::endl;

			threads.erase(boost::this_thread::get_id());

		}));
	
		threads[thread->get_id()] = thread;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(100));

	}

	std::map<boost::thread::id, boost::shared_ptr<boost::thread>>::iterator itr;
	for(itr = threads.begin(); itr != threads.end(); itr++)
	{
		itr->second->interrupt();
		itr->second->join();
	}

	return 0;
}
